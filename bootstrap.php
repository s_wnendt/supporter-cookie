<?php

require_once __DIR__ . '/vendor/autoload.php';

try {
    (new \ew\supporterCookie\Supporter())
        ->apply(
            __DIR__ . '/supporter.xml',
            __DIR__ . '/runtime/log'
        );
} catch (Exception $e) {
}
