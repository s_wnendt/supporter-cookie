# Supporter Cookie

Assigns a supporter record via cookie.

- Cookie Name: `sc`
- Cookie Value: *supporter record* as JSON array
- Logfile path: `./runtime/log`
- No cookie for crawlers

Supporters are randomly selected by probability multiplier.

## Installation

```bash
composer install
```

## Configuration

... via XML File `./supporter.xml`.

Required fields:

- `supporter.supporter_id`
- `supporter.multiplicator`

**Example**

```xml
<?xml version="1.0" encoding="utf8"?>
<supporters>
    <supporter>
        <ew_supporter_name>Name Nachname</ew_supporter_name>
        <ew_supporter_mail>mail@example.com</ew_supporter_mail>
        <supporter_id>abc1</supporter_id>
        <ew_supporter_phone>+49 (0) 123456789</ew_supporter_phone>
        <ew_supporter_image>abc1.png</ew_supporter_image>
        <multiplicator>150</multiplicator>
    </supporter>
    <supporter>
        <ew_supporter_name>Name Nachname</ew_supporter_name>
        <ew_supporter_mail>mail2@example.com</ew_supporter_mail>
        <supporter_id>abc2</supporter_id>
        <ew_supporter_phone>+49 (0) 123456789</ew_supporter_phone>
        <ew_supporter_image>abc2.png</ew_supporter_image>
        <multiplicator>125</multiplicator>
    </supporter>
</supporters>
```

## Usage

See example in `./bootstrap.php`. This can of course be included.
