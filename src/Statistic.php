<?php


namespace ew\supporterCookie;


class Statistic
{
    const LOG_FILE_NAME = 'supporter-cookie.log';

    /**
     * @param string $logDir
     * @param array  $data
     * @param string $separator
     * @return bool
     */
    public function add($logDir, array $data, $separator = ';')
    {
        $entry = [
            'datetime' => date('Y-m-d H:i:s'),
            'supporter' => isset($data['supporter_id']) ? $data['supporter_id'] : '',
            'hit' => 1,
            'uri' => $_SERVER['REQUEST_URI'],
        ];
        if (($file = $this->getLogFilePath($logDir)) === null) {
            return false;
        }
        if (!file_exists($file)) {
            if (file_put_contents($file, implode($separator, array_keys($entry)) . PHP_EOL, LOCK_EX) === false) {
                return false;
            }
        }
        return file_put_contents($file, implode($separator, $entry) . PHP_EOL, FILE_APPEND | LOCK_EX) !== false;
    }

    /**
     * @param string $logDir
     * @param string $logFileName
     * @return null|string
     */
    protected function getLogFilePath($logDir, $logFileName = self::LOG_FILE_NAME)
    {
        $logDir = rtrim(trim($logDir), '/');
        if (!is_dir($logDir) && !mkdir($logDir, 0777, true)) {
            return null;
        }
        return $logDir . '/' . date('Y-m') . ".{$logFileName}";
    }
}
