<?php

namespace ew\supporterCookie;

use Jaybizzle\CrawlerDetect\CrawlerDetect;

/**
 * Class Client
 *
 * @package ew\supporterCookie
 */
class Client
{
    /**
     * Cookie name
     */
    const COOKIE_NAME = 'sc';

    /**
     * @param array  $possibleDataPool
     * @param string $name
     * @return bool
     */
    public function isCookieValid(array $possibleDataPool, $name = self::COOKIE_NAME)
    {
        if (($value = $this->getCookie($name)) === null) {
            return false;
        }
        return in_array($value, $possibleDataPool, true);
    }

    /**
     * @param string $name
     * @return array|null
     */
    public function getCookie($name = self::COOKIE_NAME)
    {
        if (!isset($_COOKIE[$name]) || !is_string($_COOKIE[$name]) || empty($_COOKIE[$name])) {
            return null;
        }
        $data = json_decode($_COOKIE[$name], true);
        return is_array($data) && !empty($data) ? $data : null;
    }

    /**
     * @param array  $data
     * @param string $name
     * @param int    $expire defaults to 2147483647 (03:14:07 UTC 19. Januar 2038)
     * @return bool
     */
    public function setCookie(array $data, $name = self::COOKIE_NAME, $expire = 2147483647)
    {
        return setcookie($name, json_encode($data), $expire, '/');
    }

    /**
     * @return bool
     */
    public static function isBot()
    {
        $detect = new CrawlerDetect();

        return $detect->isCrawler();
    }
}
