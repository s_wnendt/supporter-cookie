<?php

namespace ew\supporterCookie;

use Exception;

/**
 * Class Supporter
 *
 * @package ew\supporterCookie
 */
class Supporter
{
    /**
     * @var null|array
     */
    private $config = null;

    /**
     * @var null|null
     */
    private $supporters = null;

    /**
     * @param $configFile
     * @param $logDir
     * @return bool
     * @throws Exception
     */
    public function apply($configFile, $logDir)
    {
        if (Client::isBot()) {
            return false;
        }
        if (($config = $this->getConfig($configFile)) === null) {
            throw new Exception("Could not fetch configuration from {$configFile}");
        }
        if (($supporters = $this->getSupporters($config)) === null) {
            throw new Exception('No supporters found in configuration');
        }
        $client = new Client();
        if ($client->isCookieValid($supporters)) {
            return true;
        }
        return $this->setSupporter($client, $supporters, $logDir);
    }

    /**
     * @param Client $client
     * @param array  $supporters
     * @param string $logDir
     * @return bool
     */
    protected function setSupporter(Client $client, array $supporters, $logDir)
    {
        $supporters = array_values($supporters);
        $chosen = mt_rand(0, count($supporters) - 1);
        $supporter = $supporters[$chosen];
        if (!$client->setCookie($supporter)) {
            return false;
        }
        $statistic = new Statistic();
        return $statistic->add($logDir, $supporter);
    }

    /**
     * @param $config
     * @return array|null
     */
    protected function getSupporters($config)
    {
        if ($this->supporters === null) {
            if (!isset($config['supporter'])) {
                return null;
            }
            $supporters = [];
            foreach ($config['supporter'] as $s) {
                if (!isset($s['multiplicator']) || (int)$s['multiplicator'] < 1) {
                    $s['multiplicator'] = 1;
                }
                $s['multiplicator'] = (int)$s['multiplicator'];
                $s['_etag'] = isset($config['mtime']) ?
                    ((int)$config['mtime'] * 2.78 / 2 + 99.79375) :
                    0;
                for ($i = 1; $i <= $s['multiplicator']; $i++) {
                    $supporters[] = $s;
                }
            }
            $this->supporters = !empty($supporters) ? $supporters : null;
        }
        return $this->supporters;
    }

    /**
     * @param $file
     * @return array|null
     */
    protected function getConfig($file)
    {
        if ($this->config === null) {
            if (!file_exists($file)) {
                return null;
            }
            $content = file_get_contents($file);
            if ($content === false || !is_string($content) || empty($content)) {
                return null;
            }
            $config = self::xmlToArray($content);
            if (!is_array($config) || empty($config)) {
                return null;
            }
            $this->config = array_merge($config, [
                'mtime' => filemtime($file),
            ]);
        }
        return $this->config;
    }

    /**
     * @param $xmlString
     * @return mixed
     */
    protected static function xmlToArray($xmlString)
    {
        return json_decode(json_encode(simplexml_load_string($xmlString)), true);
    }
}
